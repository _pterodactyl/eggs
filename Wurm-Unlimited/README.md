# Wurm Unlimited

Steam Description  
Wurm Unlimited is the standalone version of the fantasy sandbox world Wurm Online,
the MMORPG where the players are in charge!
A pioneer in the ideas of player influence, crafting and adventure,
it is now one of the most deep and feature packed true sandbox experiences available.

### Server Ports
game port (default 8907) (externalport)

| Port    | default |
|---------|---------|
| Game    | 3724    |